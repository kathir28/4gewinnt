lib = window.lib || {};

(function () {
    class  SimpleView{
        /**
         * param model
         * param dom where to append output
         * constructor
         * return nothing
         */
        constructor(model, dom){
            // get model and dom
            this._model = model;
            this._dom = dom;

            // add clickhandler
            this._clickhandler();

            // add other event listener
            $(model).on('change', this._render.bind(this));
            $(model).on('gameOver', this._gameOver.bind(this, 'GAME OVER'));
            $(model).on('playerWon', this._gameOver.bind(this, this._model.getCurrentPlayerName() + ' wins!'));
        }

        /**
         * no params
         * output of game
         * return nothing
         */
        _render(){
            // remove current output
            this._dom.empty();

            // output of current player
            this._dom.append('<p class="text">Current Player: ' + this._model.getCurrentPlayerName() + '</p>');

            // get string
            let string = this._model.toString();

            // get column headers
            let numbers = "";
            for(let i=1; i<=this._model.width; i++){
                numbers += i;
            }

            // append column headers, string and restart button to dom
            this._dom.append('<p>'+numbers+'</p>');
            this._dom.append('<p>'+string+'</p>');
            this._dom.append('<button id="restart">Restart</button>');
        }

        /**
         * param text to append at dom
         * output of game when game over
         * return nothing
         */
        _gameOver(text){
            // remove current output
            this._dom.empty();

            // get string and append it to dom
            let string = this._model.toString();
            this._dom.append('<p>'+string+'</p>');

            // append message and restart button to dom
            this._dom.append(
                '<p class="text">'+text+'</p>' // TODO
                + '<button id="restart">Restart</button>'
            );
        }

        /**
         * no parameters
         * adds event listener to keyup and click on restart button
         * return nothing
         */
        _clickhandler(){
            // event listener to keyup event
            document.addEventListener("keyup", (evt) => {
                // convert the keyCode of the pressed key to the number for which it stands
                let index = Number(String.fromCharCode(evt.keyCode)) - 1;
                // ask model if insert of token is possible at pressed index
                if(this._model.isInsertTokenPossibleAt(index))
                    // if yes - insert token at pressed index
                    this._model.insertTokenAt(index);
            });

            // event listener to click event on restart button
            this._dom.on("click", "#restart", () => {
                // reload game
                this._model.init();
            });
        }

        _hide(){
            this._dom.hide();
        }

        _show(){
            this._dom.show();
        }
    }

    class StartDisplay{
        /**
         * param model
         * param dom where to append output
         * constructor
         * return nothing
         */
        constructor(model, dom){
            // get model and dom
            this._model = model;
            this._dom = dom;


            // add clickhandler
            this._clickhandler();

            // add other event listener
            //$(model).on('change', this._render.bind(this));
            //$(model).on('gameOver', this._gameOver.bind(this, 'GAME OVER'));
            //$(model).on('playerWon', this._gameOver.bind(this, this._model.getCurrentPlayerName() + ' wins!'));

        }

        /**
         * no params
         * output of game
         * return nothing
         */
        _render(){
            let html = "<h1>4Gewinnt</h1>"+
                "<form class='playerOptions'>"+
                "<label for='p1name'>Spieler 1: </label>"+
                "<input type='text' id='p1name' size='20'/>"+
                "<label for='p2name'>Spieler 2: </label>"+
                "<input type='text' id='p2name' size='20'/>"+
                "<input type='submit' value='Start' id='start'/>"+
                "</form>";
            this._dom.append(html);
        }

        _clickhandler(){
            // event listener to click event on start button
            this._dom.on("click", "#start", () => {
                // read options
                let p1name = $('#p1name').val();
                let p2name = $('#p2name').val();
                // reload game
                this._model.init(p1name, p2name);
            });
        }
    }

    lib.SimpleView = SimpleView;
    lib.StartDisplay = StartDisplay;
}());
