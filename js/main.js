$(function () {
    let model = new lib.GameModel();
    let simple = $('.simpleGame');
    let start = $('#startDisplay');

    let simpleView = new lib.SimpleView(model, simple);
    let startDisplay = new lib.StartDisplay(model, start);

    startDisplay._render();
    simpleView._hide();
});
